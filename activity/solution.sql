[1]
SELECT customerName FROM customers WHERE country = "Philippines";

[2]
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

[3]
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

[4]
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

[5]
SELECT customerName FROM customers WHERE state IS NULL;

[6]
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

[7]
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

[8]
SELECT orderNumber FROM orders WHERE comments LIKE "%DHL%";

[9]
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

[10]
SELECT DISTINCT country FROM customers;
-- SELECT country FROM customers GROUP BY country HAVING COUNT(*) = 1;

[11]
SELECT DISTINCT status FROM orders;
-- SELECT status FROM orders GROUP BY status HAVING COUNT(*) = 1;

[12]
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

[13]
SELECT employees.firstName, employees.lastName, offices.city FROM employees JOIN offices ON offices.officeCode = employees.officeCode WHERE offices.officeCode = 5;

[14]
SELECT customers.customerName FROM customers JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE employees.employeeNumber = 1166;

[15] Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT products.productName, customers.customerName FROM customers JOIN orders ON orders.customerNumber = customers.customerNumber JOIN orderdetails ON orderdetails.orderNumber = orders.orderNumber JOIN products ON products.productCode = orderdetails.productCode WHERE customers.customerName = "Baane Mini Imports";

[16]
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees JOIN customers ON customers.salesRepEmployeeNumber = employees.employeeNumber JOIN offices ON offices.officeCode = employees.officeCode WHERE customers.country = offices.country;

[17]
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

[18]
SELECT customerName FROM customers WHERE phone LIKE "%+81%";